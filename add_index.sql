create index index_item_on_created_at on items (created_at);
create index index_item_on_category_id on items (category_id);
create index index_item_on_seller_id on items (seller_id);
